import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { User } from '../interfaces/user';
import { UserHttpClientService } from './user-http-client.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User | null>;

  constructor(private http: UserHttpClientService) {
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser') as string));
      this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
      return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.http.login({username, password} as User)
    .subscribe((res: User) => {
      if (res && res.token) {
        localStorage.setItem('currentUser', JSON.stringify(res));
        this.currentUserSubject.next(res);
      }
      return res;
    });
  }

  logout() {
      localStorage.removeItem('currentUser');
      this.currentUserSubject.next({} as User);
  }
}