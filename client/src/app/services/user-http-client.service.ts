import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { User } from '../interfaces/user';
import { Observable } from 'rxjs/internal/Observable';
const serverUrl = 'http://localhost:3000/';
const loginUrl = serverUrl + 'login';
@Injectable({
  providedIn: 'root'
})

export class UserHttpClientService {

  constructor(private http: HttpClient) {}

  register(user: User) {
    return this.http.post(serverUrl + loginUrl + '/register', user);
  }

  login(user: User): Observable<User> {
    return this.http.post<User>(serverUrl + loginUrl, user);
  }
}
