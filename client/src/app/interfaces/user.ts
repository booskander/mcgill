export interface User {
  username: string;

  name: string;

  lastName: string;

  email: string;

  password: string;

  token: string;
}
