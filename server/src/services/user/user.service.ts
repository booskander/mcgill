import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserDocument, User } from 'src/model/database/user-document';
@Injectable()
export class UserService {
  constructor(@InjectModel('User') public userModel: Model<UserDocument>) {}

  async getAllUsers(): Promise<UserDocument[]> {
    return this.userModel.find({});
  }

  async getUser(username: string): Promise<User> {
    return this.userModel.findOne({ username });
  }

  async addUser(user: User): Promise<UserDocument> {
    try {
      return this.userModel.create(user);
    } catch (error) {
      return Promise.reject(`Failed to insert user: ${error}`);
    }
  }

  async deleteUser(username: string): Promise<User> {
    try {
      return this.userModel.findOneAndDelete({ username });
    } catch (error) {
      console.error(error);
    }
  }
}
