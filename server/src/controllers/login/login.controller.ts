import { Body, Controller, Get, Post } from '@nestjs/common';
import { User } from 'src/model/database/user-document';
import { UserLoginDto } from 'src/model/database/user-login-dto';
import { UserService } from 'src/services/user/user.service';

@Controller('login')
export class LoginController {
  constructor(private userService: UserService) {}

  @Post()
  async login(@Body() body: UserLoginDto) {
    try {
      const user: User = await this.userService.getUser(body.username);

      if (body.password !== user.password) {
        return Promise.reject('Invalid password');
      }
      return user;
    } catch (error) {
      return Promise.reject(`Failed to login: ${error}`);
    }
  }

  @Post('/register')
  async register(@Body() body: User) {
    try {
      const user: User = await this.userService.addUser(body);
      return user;
    } catch (error) {
      return Promise.reject(`Failed to register: ${error}`);
    }
  }
}
